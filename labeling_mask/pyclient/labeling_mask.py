import requests
from io import BytesIO
from PIL import Image


class labeling_mask():
    def __init__(self, project={'api_root': ''}):
        self.project = project

    def list(self, path: str = ''):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'labeling_mask/list/' + \
            str(self.project['name']) + path
        data = requests.get(url).json()
        ret = {'images': data['images'], 'masks': data['masks']}
        return ret

    def tuples(self, path: str = ''):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + 'labeling_mask/tuples/' + \
            str(self.project['name']) + path
        data = requests.get(url).json()
        ret = [(t[0], t[1]) for t in data['tuples']]
        return ret

    def download(self, path: str):
        if not path.startswith('/'):
            path = '/'+path
        url = self.project['api_root'] + \
            'file/download/' + str(self.project['name']) + path
        response = requests.get(url)
        img = Image.open(BytesIO(response.content))
        return img

    def download_tuples(self, path: str = ''):
        ret = []
        for t in self.tuples(path):
            img = self.download(path+'/'+t[0])
            mask = self.download(path+'_masks'+'/'+t[1])
            ret.append((img, mask))
        return ret
