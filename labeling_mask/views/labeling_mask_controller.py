import os
import json
from flask import current_app, make_response, request, jsonify, abort, send_file
from flask_unchained import BundleConfig, Controller, route, injectable
from backend.config import Config as AppConfig
from io import StringIO, BytesIO
from PIL import Image
from base64 import decodestring, b64decode
import re
from werkzeug.utils import secure_filename

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'tif'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class LabelingMaskController(Controller):

    @route('/upload/<string:project>/<string:filename>', methods=['POST'])
    def root_image_upload(self, project, filename):
        return self.image_upload(project, '', filename)

    @route('/upload/<string:project>/<path:path>/<string:filename>', methods=['POST'])
    def image_upload(self, project, path, filename):
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        data = request.json['data']
        # source: https://github.com/python-pillow/Pillow/issues/3400
        base64_data = re.sub('^data:image/.+;base64,', '', data)
        byte_data = b64decode(base64_data)
        image_data = BytesIO(byte_data)
        img = Image.open(image_data)
        filename, _ = os.path.splitext(filename)
        filename_mask = filename + '_mask.png'
        os.makedirs(os.path.join(BASE_DIR, path+'_masks'), exist_ok=True)
        filepath = os.path.join(BASE_DIR, path+'_masks', filename_mask)
        img.save(filepath, 'PNG')
        return jsonify(success=True)

    @route('/list/<string:project>', defaults={'req_path': ''})
    @route('/list/<string:project>/<path:req_path>')
    def files_listing(self, project, req_path):
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        abs_path = os.path.join(BASE_DIR, req_path)
        if not os.path.exists(abs_path):
            return abort(404)
        if os.path.isfile(abs_path):
            resp = jsonify(project=project,
                           path=req_path)
        else:
            files = list(
                filter(lambda f: allowed_file(f), os.listdir(abs_path)))
            images = []
            masks = []
            for f in files:
                if not '_mask' in f:
                    images.append(f)
                else:
                    masks.append(f)
            if os.path.exists(abs_path+'_masks'):
                files = list(
                    filter(lambda f: allowed_file(f), os.listdir(abs_path+'_masks')))
                masks.append(f)
            resp = jsonify(project=project,
                           path=req_path, images=images, masks=masks)
        return resp

    @route('/tuples/<string:project>', defaults={'req_path': ''})
    @route('/tuples/<string:project>/<path:req_path>')
    def tuples_listing(self, project, req_path):
        BASE_DIR = os.path.join(AppConfig.DATA_FOLDER, project)
        abs_path = os.path.join(BASE_DIR, req_path)
        masks_path = abs_path + '_masks'
        if not os.path.exists(abs_path):
            return abort(404)
        if os.path.isfile(abs_path):
            resp = jsonify(project=project,
                           path=req_path)
        else:
            files = list(
                filter(lambda f: allowed_file(f), os.listdir(abs_path)))
            files_masks = list(
                filter(lambda f: allowed_file(f), os.listdir(masks_path)))
            images = []
            masks = []
            tuples = []
            for f in files:
                for f_m in files_masks:
                    filename, _ = os.path.splitext(f)
                    if f_m.startswith(filename):
                        images.append(f)
                        masks.append(f_m)
                        tuples.append((f, f_m))
            resp = jsonify(project=project,
                           path=req_path, images=images, masks=masks, tuples=tuples)
        return resp
