"""
    labeling_mask
    ~~~~

    Component for labeling_mask in general.

    :copyright: Copyright © 2019 chriamue
    :license: Not open source, see LICENSE for details
"""

__version__ = '0.1.0'


from flask_unchained import Bundle


class LabelingMaskBundle(Bundle):
    command_group_names = ['labeling_mask']
