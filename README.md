# labeling_mask

Component for labeling images to a mask.

## Installation

```yml
# zillean-domain.yml

name: domain
comps:
 - https://gitlab.com/zilleanai/comp/labeling_mask
```

```py
# unchained_config.py

BUNDLES = [
    'flask_unchained.bundles.api',
...
    'bundles.project',
    'bundles.labeling_mask',
...
    'backend',  # your app bundle *must* be last
]
```

```py
# routes.py

routes = lambda: [
    include('bundles.project.routes'),
...
    include('bundles.labeling_mask.routes'),
...
    controller(SiteController), 
]
```

```js
// routes.js
import {
  LabelingMask,
  LabelingMaskPainter,
} from 'comps/labeling_mask/pages'
...
export const ROUTES = {
  Home: 'Home',
  ...
  LabelingMask: 'LabelingMask',
  LabelingMaskPainter: 'LabelingMaskPainter',
  ...
}
...
const routes = [
  {
    key: ROUTES.Home,
    path: '/',
    component: Home,
  },
  ...
  {
    key: ROUTES.LabelingMask,
    path: '/labeling_mask/:subpath',
    component: LabelingMask,
  },
  {
    key: ROUTES.LabelingMaskPainter,
    path: '/labeling_mask/:path/:file',
    component: LabelingMaskPainter,
  },
  ...
]
```

```js
// NavBar.js
<div className="menu left">
    <NavLink to={ROUTES.Projects} />
    ...
    <NavLink to={ROUTES.LabelingMask} params={ { subpath: '/' }} />
    ...
</div>
```

## pyclient

```python
import os
domain = os.environ.get('ZILLEAN_DOMAIN') or 'zillean.ai'
api_root = 'https://'+domain+'/api/v1/'
from zillean_client import load
load(api_root)
from zillean_client.labeling_mask import labeling_mask
api = labeling_mask(project={'api_root': api_root, 'name': 'test'})
print(api.list())
print(api.tuples(''))
```
