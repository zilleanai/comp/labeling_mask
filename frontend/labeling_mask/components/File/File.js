import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { NavLink } from 'components'
import { ROUTES } from 'routes'
import { v1 } from 'api'
import { storage } from 'comps/project'
import './file.scss'

class File extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      deleted: false
    }
  }

  render() {
    const { file, path, pristine, submitting } = this.props
    const { deleted } = this.state
    if (!file || !path || deleted) {
      return null
    }
    return (
      <div className='file'>
        <NavLink to={ROUTES.LabelingMaskPainter} params={{path, file}}>
          {file}
        </NavLink>
      </div>
    )
  }
}

export default compose(
)(File)
