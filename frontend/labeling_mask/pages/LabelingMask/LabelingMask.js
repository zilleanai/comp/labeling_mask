import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { PageContent } from 'components'
import { v1 } from 'api'
import { storage } from 'comps/project'
import { FileList, DirList } from 'comps/file/components'
import { File, Dir } from '../../components'
import './labeling-mask.css'

class LabelingMask extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  fileDecorator = (file, path, i) => {
    const ext = ['.bmp', '.png', '.jpg', '.tif', '.tiff']
    return ext.map((e, i) => {
      if (file.endsWith(e) || file.endsWith(e.toUpperCase())) {
        return (
          <li key={i}>
            <File path={path} file={file} />
          </li>
        )
      }
    })
  }

  dirDecorator = (dir, path, i) => {
    return (
      <li key={i}>
        <Dir path={path} dir={dir} />
      </li>
    )
  }

  render() {
    const { subpath } = this.props
    return (
      <PageContent>
        <Helmet>
          <title>Labeling Mask</title>
        </Helmet>
        <h1>Labeling Mask</h1>
        <DirList path={subpath} decorator={this.dirDecorator} />
        <FileList path={subpath} decorator={this.fileDecorator} />
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const subpath = decodeURIComponent(props.match.params.subpath)
    return {
      subpath
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)

export default compose(
  withConnect
)(LabelingMask)
