import React from 'react'
import Helmet from 'react-helmet'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { bindRoutineCreators } from 'actions'
import { PageContent } from 'components'
import { get, post } from 'utils/request'
import { v1 } from 'api'
import { storage } from 'comps/project'
import CanvasDraw from "react-canvas-draw";
import './labeling-mask-painter.css'

class LabelingMaskPainter extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      labelClass: 1,
      coloredClasses: false,
      color: "#ffc600",
      width: 800,
      height: 800,
      img: null,
      imgWidth: 640,
      imgHeight: 480,
      brushRadius: 10,
      lazyRadius: 12,
      loading: true
    };

    this.saveableCanvas = null;
  }

  // source: https://stackoverflow.com/questions/3426404/create-a-hexadecimal-colour-based-on-a-string-with-javascript
  intToRGB(i) {
    var c = ((i) & 0x00FFFFFF)
      .toString(16)
      .toUpperCase();
    return "00000".substring(0, 6 - c.length) + c;
  }

  updateImage = (backgroundImg) => {
    this.setState({ img: null })
    var img = new Image();
    img.onload = () => {
      this.setState({ imgWidth: img.width, imgHeight: img.height, backgroundImg: img.src })
    }
    img.src = backgroundImg
    this.setState({ img })
    //setTimeout(() => this.setState({ loading: false }), 1000)
  }

  componentWillReceiveProps(nextProps) {
    const { backgroundImg } = nextProps
    if (nextProps.backgroundImg !== this.props.backgroundImg) {
      //this.updateImage(backgroundImg)
    }
  }

  componentWillMount() {
    const { backgroundImg } = this.props
    this.updateImage(backgroundImg)
  }

  render() {
    const { path, file, backgroundImg } = this.props
    const { loading, labelClass, coloredClasses, brushRadius, lazyRadius, width, height, img, imgWidth, imgHeight } = this.state
    const color = ("#" + this.intToRGB(coloredClasses ? (labelClass * 0xa15f87) : labelClass))
    if (!backgroundImg || !imgWidth || !imgHeight) {
      return null
    }
    return (
      <PageContent>
        <Helmet>
          <title>Labeling Mask</title>
        </Helmet>
        <h1>Labeling Mask</h1>
        <div className="ToolBox">
          <div>
            <label>Colored:</label>
            <input type="checkbox" onClick={(e) => {
              this.setState({ coloredClasses: e.target.checked });
            }} value={coloredClasses}></input>
          </div>
          <div>
            <label>Class:</label>
            <input
              type="number"
              value={this.state.labelClass}
              onChange={e =>
                this.setState({ labelClass: parseInt(e.target.value, 10) })
              }
            />
          </div>
          <div>
            <label>Brush-Radius:</label>
            <input
              type="number"
              value={this.state.brushRadius}
              onChange={e =>
                this.setState({ brushRadius: parseInt(e.target.value, 10) })
              }
            />
          </div>
          <button
            onClick={() => {
              this.saveableCanvas.undo();
            }}
          >
            Undo
          </button>
          <button
            onClick={() => {
              var data = this.saveableCanvas.canvas.drawing.toDataURL("image/png")
              const url = v1(`/labeling_mask/upload/${storage.getProject()}${(path == '/' ? '' : path)}/${file}`)
              post(url, { data, imgWidth, imgHeight })
                .then(function (res) { return res.json(); })
                .then(function (data) { alert(JSON.stringify(data)) })
            }}
          >
            Save
          </button>
        </div>
        <CanvasDraw
          imgSrc={backgroundImg}
          loadTimeOffset={1}
          immediateLoading={true}
          hideGrid={false}
          brushColor={color}
          brushRadius={brushRadius}
          lazyRadius={lazyRadius}
          canvasWidth={imgWidth}
          canvasHeight={imgHeight}
          ref={canvasDraw => (this.saveableCanvas = canvasDraw)}
        />
      </PageContent>
    )
  }
}

const withConnect = connect(
  (state, props) => {
    const path = decodeURIComponent(props.match.params.path)
    const file = decodeURIComponent(props.match.params.file)
    const backgroundImg = v1(`/file/download/${storage.getProject()}${(path == '/' ? '' : path)}/${file}`)

    return {
      path,
      file,
      backgroundImg
    }
  },
  (dispatch) => bindRoutineCreators({}, dispatch),
)

export default compose(
  withConnect
)(LabelingMaskPainter)
